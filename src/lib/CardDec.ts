export class Card {
  constructor(
    public suit: string,
    public rank: string,)
  {
    function getScore(){

      if (suit === "K" || suit === "Q" || suit === "J") {
            return 10;
      }
      if (suit === 'A') {
            return 11;
      } else{
       return alert('Incorrect')
      }

      switch(suit) {
        case 'diams':
          return '♦';
        case 'hearts':
          return '♥';
        case 'clubs':
          return '♣';
        case 'spades':
          return '♠';
      }

      switch(rank){
          case '2':
            return 2;
          case '3':
            return 3;
          case '4':
            return 4;
          case '5':
            return 5;
          case '6':
            return 6;
          case '7':
            return 7;
          case '8':
            return 8;
          case '9':
            return 9;
          case '10':
            return 10;
        }
      }
    }
  }

type oneCardType = {
  rank: string;
  suit: string;
}

export class CardDeck {
  cards: Card[] = [];
  rank: string[] = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
  suit: string[] = ['diams', 'hearts', 'clubs', 'spades'];

  constructor() {
    for (let i = 0; i < this.rank.length; i++) {
      for (let j = 0; j < this.suit.length; j++) {
        let newObject: oneCardType = {
          rank: this.rank[i],
          suit: this.suit[j],
        }
        this.cards.push(newObject);
        console.log(this.cards)

      }
    }

    // getCard(){
    //
    //   function eraseCard (){
    //     let anyCardOfStack = Math.floor(Math.random() * 52) + 1;
    //
    //     for (let i = this.car.length; i--){
    //       if (Card[i] === Card){
    //         Card.splice(i,1);
    //       }
    //     }
    //
    //
    //   }
    // }
    //
    // getCards(2)
    // {
    //   for (let i = 0; i < 2; i++) {
    //
    //   }
    // }

  }
}

