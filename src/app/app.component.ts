import { Component } from '@angular/core';
import {CardDeck} from "../lib/CardDec";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  vision = new CardDeck();
}
